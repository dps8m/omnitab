#!/bin/bash
function t () {
  ../omnitab < $1.omn > $1.lst.new
  if ! cmp -s $1.lst $1.lst.new; then
    xxdiff $1.lst $1.lst.new
  fi
}
t $1
