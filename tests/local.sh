#!/bin/bash
function t () {
  ../omnitab < $1.omn > $1.lst.new
  if ! cmp -s $1.lst $1.lst.new; then
    xxdiff $1.lst $1.lst.new
  fi
}
t test01
t test02
t test03
t test04
t test05
t test06
t test07
t test08
t test09
t test10
t test11
t test12
t test13
t test14
t test15
t test16
t test17
t test18
t test19
t test20
t test21
t test22
t test23
t test24
t test25
t test26
t test27
t test28
t test29
t test30
t test31
t test32
t test33
t test34
t test35
t test36
t test37
t test38
t test39
t test40
t test41
t testf01
t testf02
t testf03
t testf04
t testf05
t testf06
t testf07
t testf08
t testf09
t testf10
t testf11
