Port of OMNITAB to Multics.

Upload oi\*.fortran and Makefile to Multics. "make clean all" should generate
an omnitab executable with a single warning about an unset variable 'args1'.

The file test1.omnitab is an OMNITAB program for the OMNITAB users manual. Run
omnitab and paste the contents of test1.omnitab in; omnitab will fail with many
run-time error messages.

The first error (wrong number of arguments to SET) appears to be caused by
EXPAND not scanning the command line for arguments due to argument J being
equal to 1.  

Files

localmake.sh: catenate the source into a single file and compile with gfortran.  
Makefile: Multics makefile.  
o01.fortran - o17.fortran: OMNITAB source.  
orig/: Files from the OMNITAB distribution.  
README.md: This file.  
test1.omnitab: The example OMNITAB program from page 3 of orig/omnitabiiusersre552hogb.pdf.  


