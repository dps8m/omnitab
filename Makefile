all : omnitab

omnitab : o01 o02 o03 o04 o05 o06 o07 o08 o09 o10 o11 o12 o13 o14 o15 o16 o17
	le o01 o02 o03 o04 o05 o06 o07 o08 o09 o10 o11 o12 o13 o14 o15 o16 o17 -of omnitab

o01 : o01.fortran
	ft o01 -card -static

o02 : o02.fortran
	ft o02 -card -static

o03 : o03.fortran
	ft o03 -card -static

o04 : o04.fortran
	ft o04 -card -static

o05 : o05.fortran
	ft o05 -card -static

o06 : o06.fortran
	ft o06 -card -static

o07 : o07.fortran
	ft o07 -card -static

o08 : o08.fortran
	ft o08 -card -static

o09 : o09.fortran
	ft o09 -card -static

o10 : o10.fortran
	ft o10 -card -static

o11 : o11.fortran
	ft o11 -card -static

o12 : o12.fortran
	ft o12 -card -static

o13 : o13.fortran
	ft o13 -card -static

o14 : o14.fortran
	ft o14 -card -static

o15 : o15.fortran
	ft o15 -card -static

o16 : o16.fortran
	ft o16 -card -static

o17 : o17.fortran
	ft o17 -card -static

clean :
	dl o01 o02 o03 o04 o05 o06 o07 o08 o09 o10 o11 o12 o13 o14 o15 o16 o17 omnitab
